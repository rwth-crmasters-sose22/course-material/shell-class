FROM golang:1.12.4-alpine as build

RUN apk add --no-cache git

WORKDIR /
RUN git clone https://github.com/mstruebing/tldr.git tldr

WORKDIR /tldr
RUN GO111MODULE=on CGO_ENABLED=0 go build -o bin/tldr cmd/tldr/main.go

FROM beevelop/nodejs-python-ruby:latest

# The Oh-My-Posh theme to use
# ENV OMP_THEME="M365Princess.omp.json"

LABEL org.opencontainers.image.authors="diraneyya@ip.rwth-aachen.de"

# This is in order to have the man pages during exercises
# RUN sed -i '/path-exclude \/usr\/share\/man/d' /etc/dpkg/dpkg.cfg.d/docker
# RUN sed -i '/path-exclude \/usr\/share\/groff/d' /etc/dpkg/dpkg.cfg.d/docker
# RUN apt update && apt install -y man git curl && apt install --reinstall coreutils
# omitted unzip and wget
RUN apt update && apt install -y git curl bash unzip

RUN git config --global init.defaultBranch master

WORKDIR /tldr/
COPY --from=build /tldr/bin/tldr /bin/

# The DATA_PATH is used for the deliverables, or the submissible
# content for the classroom activity.
ENV DATA_PATH="/data"

ENV HISTORY_OUTPUT="$DATA_PATH/history.txt"

RUN mkdir -p $DATA_PATH
ADD . $REPO_PATH
WORKDIR $REPO_PATH

WORKDIR /root
ADD https://starship.rs/install.sh .
RUN sh /root/install.sh -y

# ADD https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 /usr/local/bin/oh-my-posh
# RUN chmod +x /usr/local/bin/oh-my-posh

# ADD https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip /root/.poshthemes/themes.zip
# RUN unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
# RUN chmod u+rw ~/.poshthemes/*.json
# RUN rm ~/.poshthemes/themes.zip

# The bash login script in below clears the history and restores
# progress using the contents of the $GITHUG_PROFILE_OUTPUT file.
RUN printf "tldr git 1>/dev/null \nalias man=\"tldr\" \n\
history -c\nHISTSIZE= \nHISTFILESIZE= \n\
echo '--- NEW SESSION ---' >> $HISTORY_OUTPUT \n\
echo -e '\nIMPORTANT: everything you type in this container is \
recorded to assist in the grading process.' \n\
eval \"\$(starship init bash)\" \n" >> ~/.bash_profile
# eval \"\$(oh-my-posh init bash --config ~/.poshthemes/${OMP_THEME})\" \n" >> ~/.bash_profile

RUN printf "history -a\ncat \$HISTFILE >> $HISTORY_OUTPUT \n" >> ~/.bash_logout 

ENTRYPOINT ["/bin/bash", "--login"]
VOLUME $DATA_PATH
